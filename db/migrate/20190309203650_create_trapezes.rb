class CreateTrapezes < ActiveRecord::Migration[5.2]
  def change
    create_table :trapezes do |t|
      t.integer :lager_base
      t.integer :smaller_base
      t.integer :lateral

      t.timestamps
    end
  end
end
