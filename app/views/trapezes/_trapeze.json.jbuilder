json.extract! trapeze, :id, :lager_base, :smaller_base, :lateral, :created_at, :updated_at
json.url trapeze_url(trapeze, format: :json)
