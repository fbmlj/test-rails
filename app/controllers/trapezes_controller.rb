class TrapezesController < ApplicationController
  before_action :set_trapeze, only: [:show, :edit, :update, :destroy]

  # GET /trapezes
  # GET /trapezes.json
  def index
    @trapezes = Trapeze.all
  end

  # GET /trapezes/1
  # GET /trapezes/1.json
  def show
  end

  # GET /trapezes/new
  def new
    @trapeze = Trapeze.new
  end

  # GET /trapezes/1/edit
  def edit
  end

  # POST /trapezes
  # POST /trapezes.json
  def create
    @trapeze = Trapeze.new(trapeze_params)

    respond_to do |format|
      if @trapeze.save
        format.html { redirect_to @trapeze, notice: 'Trapeze was successfully created.' }
        format.json { render :show, status: :created, location: @trapeze }
      else
        format.html { render :new }
        format.json { render json: @trapeze.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trapezes/1
  # PATCH/PUT /trapezes/1.json
  def update
    respond_to do |format|
      if @trapeze.update(trapeze_params)
        format.html { redirect_to @trapeze, notice: 'Trapeze was successfully updated.' }
        format.json { render :show, status: :ok, location: @trapeze }
      else
        format.html { render :edit }
        format.json { render json: @trapeze.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trapezes/1
  # DELETE /trapezes/1.json
  def destroy
    @trapeze.destroy
    respond_to do |format|
      format.html { redirect_to trapezes_url, notice: 'Trapeze was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trapeze
      @trapeze = Trapeze.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trapeze_params
      params.require(:trapeze).permit(:lager_base, :smaller_base, :lateral)
    end
end
