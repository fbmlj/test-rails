class Trapeze < ApplicationRecord
  
  validate :trapeze_valid
  
  def perimeter
    lager_base + smaller_base + 2 * lateral
  end 

  def area
    base=(lager_base-smaller_base)/2
    height=(lateral*lateral-base*base)**(0.5)
    ((lager_base+smaller_base)/2)*height
  end
  def height
    base=(lager_base-smaller_base)/2
    (lateral*lateral-base*base)**(0.5)
  end

  private

  def trapeze_valid
      if lager_base
          errors.add(:lager_base, "must be greather than 0") if lager_base <= 0
          errors.add(:lager_base, "must be less than or equal to 50") if lager_base > 1000
      else
          errors.add(:lager_base, "must be present")
      end

      if smaller_base
        errors.add(:smaller_base, "must be greather than 0") if smaller_base <= 0
        errors.add(:smaller_base, "must be less than or equal to 50") if smaller_base > 1000
        if lager_base
          errors.add(:smaller_base,'must be smaller') if smaller_base > lager_base 
        end
      else
        errors.add(:smaller_base, "must be present")
        
    end

    if lateral
      errors.add(:lateral, "must be greather than 0") if lateral <= 0
      errors.add(:lateral, "must be less than or equal to 50") if lateral > 1000
    else
      errors.add(:lateral, "must be present")
    end    

    
  end
end


