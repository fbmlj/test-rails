require 'rails_helper'

RSpec.describe "trapezes/edit", type: :view do
  before(:each) do
    @trapeze = assign(:trapeze, Trapeze.create!(
      :lager_base => 1,
      :smaller_base => 1,
      :lateral => 1
    ))
  end

  it "renders the edit trapeze form" do
    render

    assert_select "form[action=?][method=?]", trapeze_path(@trapeze), "post" do

      assert_select "input[name=?]", "trapeze[lager_base]"

      assert_select "input[name=?]", "trapeze[smaller_base]"

      assert_select "input[name=?]", "trapeze[lateral]"
    end
  end
end
