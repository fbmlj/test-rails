require 'rails_helper'

RSpec.describe "trapezes/new", type: :view do
  before(:each) do
    assign(:trapeze, Trapeze.new(
      :lager_base => 1,
      :smaller_base => 1,
      :lateral => 1
    ))
  end

  it "renders new trapeze form" do
    render

    assert_select "form[action=?][method=?]", trapezes_path, "post" do

      assert_select "input[name=?]", "trapeze[lager_base]"

      assert_select "input[name=?]", "trapeze[smaller_base]"

      assert_select "input[name=?]", "trapeze[lateral]"
    end
  end
end
