require 'rails_helper'

RSpec.describe "trapezes/show", type: :view do
  before(:each) do
    @trapeze = assign(:trapeze, Trapeze.create!(
      :lager_base => 3,
      :smaller_base => 2,
      :lateral => 4
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
  end
end
