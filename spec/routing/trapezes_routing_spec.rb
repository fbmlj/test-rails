require "rails_helper"

RSpec.describe TrapezesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/trapezes").to route_to("trapezes#index")
    end

    it "routes to #new" do
      expect(:get => "/trapezes/new").to route_to("trapezes#new")
    end

    it "routes to #show" do
      expect(:get => "/trapezes/1").to route_to("trapezes#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/trapezes/1/edit").to route_to("trapezes#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/trapezes").to route_to("trapezes#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/trapezes/1").to route_to("trapezes#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/trapezes/1").to route_to("trapezes#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/trapezes/1").to route_to("trapezes#destroy", :id => "1")
    end
  end
end
