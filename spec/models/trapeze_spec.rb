require 'rails_helper'

RSpec.describe Trapeze, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"


  it "get the correct trapeze" do
    trapeze=Trapeze.create!(lateral: 5, lager_base:10, smaller_base:4)
    expect(trapeze.perimeter).to eq(24)
  end
end
