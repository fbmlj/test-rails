require 'rails_helper'

RSpec.describe "Trapezes", type: :request do
  describe "GET /trapezes" do
    it "works! (now write some real specs)" do
      get trapezes_path
      expect(response).to have_http_status(200)
    end
  end
  it "creates a trapeze and redirects to the trapeze's page" do
    get "/trapezes/new"
    expect(response).to render_template(:new)

    post "/trapezes", :params => { :trapeze => {:side => 4} }

    expect(response).to redirect_to(assigns(:trapeze))
    follow_redirect!

    expect(response).to render_template(:show)
    expect(response.body).to include("trapeze was successfully created.")
  end

  it "does not render a different template" do
    get "/trapezes/new"
    expect(response).to_not render_template(:show)
  end
end
